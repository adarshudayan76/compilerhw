# ## Exercise 1: Tokenizing

# As input, a compiler needs to read source code.  To do this, it first has to
# recognize certain pattens in the code such as names, number,
# operators, and so forth.  This step is known as tokenizing.   Implement
# the following `tokenize()` function.  It takes a string as input
# and produces a sequence of tuples of the form `(toktype, value)`
# where `toktype` is a token type and `value` is a string of
# the matching text:

# ```
# def tokenize(text):
#     ...

# assert list(tokenize("spam = x + 34 * 567")) == \
#     [ ('NAME', 'spam'), ('ASSIGN', '='), ('NAME', 'x'), 
#       ('OP', '+'), ('NUM', '34'),('OP', '*'), ('NUM', '567')]



    

text = "spam = x + 34 * 567"

    
def tokenize(text):
    tokenValue = []
    text_list = text.split(' ')
    operators = [ '+', '-', '*']
    for item in text_list:
        flag = False
        for i in item:
            if i.isalpha():
                flag = True
        if item.isnumeric():
            num = ("NUM", item)
            tokenValue.append(num)
        elif item in operators:
            op = ("OP" , item)
            tokenValue.append(op)
        elif item == '=':
            assign = ("ASSIGN" ,item)
            tokenValue.append(assign)
        elif flag:
            name = ("NAME", item)
            tokenValue.append(name)
    return tokenValue

tv = tokenize(text)
print(tv))


assert tokenize("spam = x + 34 * 567") == [ ('NAME', 'spam'), ('ASSIGN', '='), ('NAME', 'x'), ('OP', '+'), ('NUM', '34'),('OP', '*'), ('NUM', '567')]



