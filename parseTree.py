# ## Exercise 3: Tree rewriting

# Write a function that takes a tree of text strings as input and
# creates a new tree where all of the numbers have been converted
# to integers:

# ```
# def convert_numbers(tree):
#     ...

# tree = ('assign', 'spam', 
#         ('binop', '+', 
#                   ('name', 'x'),
#                   ('binop', '*', ('num', '34'), ('num', '567'))))

# assert convert_numbers(tree) == \
#     ('assign', 'spam', ('binop', '+', ('name', 'x'), ('binop', '*', ('num', 34), ('num', 567))))    
# ```

tree = ('assign', 'spam', ('binop', '+', ('name', 'x'), ('binop', '*', ('num', '34'), ('num', '567'))))


def convertToList(tree):
    return list(map(convertToList, tree)) if isinstance(tree, (tuple)) else tree
 
tree_list = convertToList(tree)

def parseTree(tree_list):  
    for item in tree_list:
        print(item)
        if isinstance(item, list):
            if len(item) == 2:
                for count, strg in enumerate(item):
                    if strg.isnumeric():
                        strg = int(strg)
                        item[count] = strg
            parseTree(item)
            pass
    return tree_list
tre = parseTree(tree_list)


def convertToTuple(tre):
    return tuple(map(convertToTuple, tre)) if isinstance(tre, (list)) else tre

tuple_with_int_values = convertToTuple(tre)

print(tuple_with_int_values)
